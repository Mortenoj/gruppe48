#pragma once

using namespace std;

#include "ListTool2B.h"
#include <fstream>
#include <string>

class Startdeltager : public NumElement {
private:
  string startnummer;
  string idnummer;
  string navn;
  string nasjon;
public:
  Startdeltager()=delete;
  Startdeltager(int deltagerID);
  Startdeltager(ifstream &innfil, int deltagerID);
  virtual void display();
  void regData(int deltagerID);
  void skrivTilFil(ofstream &utfil);
  string hentId();
  virtual ~Startdeltager();
};
