#pragma once

using namespace std;


#include "ListTool2B.h"
#include <string>
#include <fstream>


class Nasjon : public TextElement {
private:
  List* deltagerListe{};
  string navn{};          // Fult nasjonsnavn
  char* forkortelse{};    // 3-siffret landkode
  int antDeltagere{};     // Antall deltagere fra en nasjon
  string kontaktPerson{}; // Navn til kontaktperson
  int kontaktTlf{};       // TLF nr til kontaktperson
public:
  Nasjon()=delete;             // tom konstruktor
  Nasjon(char* n);
  Nasjon(ifstream &innfilNasjoner, const char* nasjonskode);
  void skrivTilFil(ofstream &utfil);     // Skriver data til nasjon til fil
  void registrerData();   // Leser data fra bruker om nasjon
  void endreData();
  virtual void display(); // Viser all data om nasjon
  ~Nasjon();
};
