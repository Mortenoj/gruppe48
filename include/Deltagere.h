#pragma once


#include "ListTool2B.h"
#include "Deltager.h"

class Deltager;

class Deltagere{
private:
  List* deltagerListe{};
  char* landskode{};
public:
  Deltagere();
  void skrivMeny();
  void skrivTilFil();
  void regDeltager();
  void endreDeltager();
  void displayDeltagere();
  void displayNasjonsDeltagere();
  void slettEnDeltager(int nr);
  void displayEnkeltDeltager();
  bool finnesDeltager(int deltagerID);
  string hentNavn(int deltagerID);
  string hentNasjon(int deltagerID);
  ~Deltagere();
};
