#pragma once

#include "ListTool2B.h"


class Grener {
private:
  char* navn{};
  List* grenListe{};
public:
  Grener();
  char hentPoengSystem(char* ovelsesID);
  void skrivMeny();
  void regGren();
  void endreGren();
  void displayGrener();
  void displayGren();
  bool finnesGren(char* t);
  void finnesGren();
  void skrivTilFil();
  ~Grener();
};
