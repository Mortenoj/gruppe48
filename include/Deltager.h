#pragma once

#include "ListTool2B.h"
#include <string>
#include <fstream>

using namespace std;

class Deltager : public NumElement {
private:
  int deltagerID{};
  string navn{};
  char* forkortelse{};
  bool kjonn{}; //0 mann, 1 dame
public:
  Deltager()=delete;
  Deltager(int id, char* t);
  Deltager(ifstream &innfilDeltagere, const int id);
  void skrivTilFil(ofstream &utfil);
  string hentLand();
  void lesFraFil();
  void registrerData();
  void endreData();
  string hentNavn();
  int hentID();
  virtual void display();
  ~Deltager();
};
