#pragma once

using namespace std;

#include "ListTool2B.h"
#include <fstream>

class Resultatdeltager : public NumElement {
private:
  int idnummer;
  int prestasjon;
  char poengSystem;
public:
  Resultatdeltager()=delete;
  Resultatdeltager(int prest, int deltagerID, bool poeng);
  Resultatdeltager(ifstream &innfil, int deltagerID);
  virtual void display();
  void skrivTilFil(ofstream &utfil);
  virtual ~Resultatdeltager();
};
