#pragma once


using namespace std;

#include "ListTool2B.h"
#include <fstream>
#include <string>

class Ovelse : public TextElement {
private:
  string ovelseNavn{};
  int klokkeslett{};
  int dato{};
  bool p;
  List* startListe{};
  List* resultatListe{};
public:
  Ovelse()=delete;
  Ovelse(char* n);
  Ovelse(ifstream &innfilGrener, const char* ovelsesID);
  void regData();
  void endreData();
  virtual void display() override;
  void endreDeltagerListe(char* ovelsesID);
  void endreDeltager();
  void fjernDeltager();
  void slettDeltagerListe(char* ovelsesID);
  void skrivTilFil(ofstream &utfil);
  void skrivStartListe(char* ovelsesID);
  void lesFraFil();
  void skrivMeny();
  void skrivStartListeMeny(char* ovelsesID);
  void skrivResultatListeMeny(char* ovelsesID, char* grenNavn);
  void skrivResultatListe();
  void skrivResultatListeTilFil(char* ovelsesID, bool p);
  void nyResultatListe(char* ovelsesID, char* grenNavn);
  void slettResultatListe(char* ovelsesID, char* grenNavn);
  void nyDeltagerListe(char* ovelsesID);
  void nyDeltager();
  void skrivStartListeTilFil(char* ovelsesID);

  virtual ~Ovelse();
};
