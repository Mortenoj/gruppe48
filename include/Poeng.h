#pragma once

#include "ListTool2B.h"
#include "Statistikk.h"

using namespace std;

//Gull: 7 poeng, så 5, 4, 3, 2 og 1

class Poeng : public Statistikk {
private:
  string poengOversikt[2][200];
public:
  Poeng();
  void oppdater(string resfil, bool poeng);
  void oppdaterSlett(string resfil, bool poeng);
  void display();
  ~Poeng();
};
