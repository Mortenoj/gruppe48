#pragma once

using namespace std;

#include <fstream>
#include "ListTool2B.h"

class Gren : public TextElement {
private:
  List* ovelsesListe{};  //Initialiserer til nullptr {}
  char* grenNavn{};
  char poengSystem{};
  int ovelser{};
public:
  Gren()=delete;
  Gren(char* n);
  Gren(ifstream &innfilGrener, const char* navn);
  char hentPoengSystem();
  void registrerData();
  void endreData();
  void skrivTilFil(ofstream &utfil);
  void lesFraFil();
  void skrivOvelsemeny(char* grenNavn);
  virtual void display();
  void displayGren();
  void regNyOvelse();
  void endreOvelse();
  void fjernOvelse();
  void startListeMeny();
  void resultatListeMeny(char* grenNavn);
  ~Gren();
};
