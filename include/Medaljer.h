#pragma once


#include "ListTool2B.h"
#include "Statistikk.h"
#include <string>
#include "Global.h"


using namespace std;

class Medaljer : public Statistikk {
private:
  string medaljeOversikt[4][200];
public:
  Medaljer();
  void oppdater(string resfil, bool poeng);
  void oppdaterSlett(string resfil, bool poeng);
  void display();
  ~Medaljer();
};
