#pragma once

using namespace std;

#include "Grener.h"
#include "Deltagere.h"
#include "Nasjoner.h"
#include "Medaljer.h"
#include "Poeng.h"
#include <string>

class Medaljer;
class Poeng;

extern Nasjoner nasjoner;
extern Deltagere deltagere;
extern Grener grener;
extern Medaljer medaljer;
extern Poeng poeng;


using namespace std;

// Globale konstanter
const int STRLEN = 80;
const int MAXKORTNAVN = 200;
const int MAXOVELSER = 20;
const int MAXDELTAGERE = 100;

// Hjelpefunksjoner
char les();
int les(char* t, int min, int max);
string les(string beskrivelse, int len);
