#pragma once


#include "ListTool2B.h"


class Nasjoner {
private:
  List* nasjonListe{};      // Liste med nasjoner
  char* navn{};
public:
  Nasjoner();
  void skrivMeny();     // Printer ut menyvalg for nasjoner
  void regNasjon();     // Registrerer ny nasjon
  void endreNasjon();
  void displayNasjoner();
  void displayDeltagere();
  bool finnesNasjon(char* t);
  void displayNasjon();
  void skrivTilFil();
  ~Nasjoner();
};
