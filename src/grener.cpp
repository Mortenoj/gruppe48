#include "Grener.h"
#include <fstream>
#include <iostream>
#include <string>
#include <cstring>
#include "ListTool2B.h"
#include "Global.h"
#include "Gren.h"


using namespace std;

Grener::Grener() {
  grenListe = new List(Sorted);

  ifstream innfilGrener("data/GRENER.DTA");
  if(innfilGrener) {
    string antGrener;
    string grenNavn;
    getline(innfilGrener, antGrener, '\n');
    cout << "antGrener: " << antGrener;

    for (size_t i = 0; i < stoi(antGrener); i++) {
      getline(innfilGrener, grenNavn, ',');
      grenListe->add(new Gren(innfilGrener, grenNavn.c_str()));
    }
  } else {
    cout << "Finner ikke filen GRENER.DTA" << endl;
  }
}

char Grener::hentPoengSystem(char* grenNavn) {
  Gren* gren = (Gren*)grenListe->remove(grenNavn);
  grenListe->add(gren);
  return gren->hentPoengSystem();
}

void Grener::skrivMeny() {
  char kommando = '0';

   while (kommando != 'X') {
     cout << "\n\nFÂLGENDE KOMMANDOER ER TILGJENGELIGE:"
     << "\n\tN - Registrer ny gren"
     << "\n\tE - Endre en gren"
     << "\n\tA - Hoveddata om alle grener"
     << "\n\tS - All data om en gren"
     << "\n\tX - Tilbake";

     kommando = les();

     switch (kommando) {
     case 'N': regGren();              break;
     case 'E': endreGren();            break;
     case 'A': displayGrener();        break;
     case 'S': displayGren();          break;
     default:                          break;
    }
  }
}

void Grener::regGren() {
  cout << "Skriv inn grenens navn: ";
  char buffer[STRLEN];
  cin.getline(buffer, STRLEN);
  navn = new char[strlen(buffer) + 1];
  strcpy(navn, buffer);
  if(grenListe->inList(navn)){
    cout << "grenen finnes fra fÃ¸r";

  } else {
    grenListe->add(new Gren(navn));
    cout << "gren ved navn: " << navn << " lagt til";
    skrivTilFil();
  }
}
void Grener::endreGren() {
  Gren* gren;

  cout << "Skriv inn grenens navn: ";
  char buffer[STRLEN];
  cin.getline(buffer, STRLEN);
  navn = new char[strlen(buffer) + 1];
  strcpy(navn, buffer);
  if(grenListe->inList(navn)){
    gren = (Gren*)grenListe->remove(navn);
    grenListe->add(gren);
    gren->endreData();
    skrivTilFil();
  } else {
    cout << "grenen du prÃ¸ver Ã¥ endre eksisterer ikke";
  }

}
void Grener::displayGrener() {
  grenListe->displayList();
}

void Grener::displayGren() {
  Gren* gren;

  cout << "Skriv inn grenens navn: ";
  char buffer[STRLEN];
  cin.getline(buffer, STRLEN);
  navn = new char[strlen(buffer) + 1];
  strcpy(navn, buffer);
  if(grenListe->inList(navn)){
    gren = (Gren*)grenListe->remove(navn);
    grenListe->add(gren);
    gren->displayGren();
  } else {
    cout << "Grenen du vil ser informasjon hos eksisterer ikke\n";
  }
}


bool Grener::finnesGren(char* t) {
  return grenListe->inList(t);
}
void Grener::finnesGren() {
  Gren* tmp;
  char* grenNavn;

  cout << "Skriv inn grenens navn: ";
  char buffer[STRLEN];
  cin.getline(buffer, STRLEN);
  grenNavn = new char[strlen(buffer) + 1];
  strcpy(grenNavn, buffer);

  if(grenListe->inList(grenNavn)){
    tmp = (Gren*)grenListe->remove(grenNavn);
    grenListe->add(tmp);
    tmp->skrivOvelsemeny(grenNavn);
  } else {
    cout << "Grenen " << grenNavn << " eksisterer ikke\n";
  }
}

void Grener::skrivTilFil() {
  Gren *gren;
  ofstream utfil("data/GRENER.DTA", ios::trunc);
  utfil << grenListe->noOfElements() << "\n";
  for (size_t i = 1; i <= grenListe->noOfElements(); i++) {
    gren = (Gren*)grenListe->removeNo(i);
    grenListe->add(gren);
    gren->skrivTilFil(utfil);
  }
}

Grener::~Grener() {
  delete[] navn;
}
