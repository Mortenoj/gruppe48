#include "Resultatdeltager.h"
#include <fstream>
#include "Global.h"
#include "ListTool2B.h"
#include <iostream>

Resultatdeltager::Resultatdeltager(int prest, int deltagerID, bool poeng) : NumElement(prest) {
  prestasjon = prest;
  idnummer = deltagerID;
  if(poeng == true) {
    poengSystem = 'P';
  } else {
    poengSystem = 'T';
  }
}

Resultatdeltager::Resultatdeltager(ifstream &innfil, int deltagerID) : NumElement(deltagerID) {
  idnummer = deltagerID;
  string tmp;
  
  getline(innfil, tmp, '\n');
  prestasjon = stoi(tmp);
}

void Resultatdeltager::display() {
  cout  << endl << "Deltagerens navn: " << deltagere.hentNavn(idnummer) << endl
        << "Deltagerens unike ID: " << idnummer << endl
        << "Deltagerens hjemland: " << deltagere.hentNasjon(idnummer) << endl;
  if(poengSystem == 'P') {
    cout << "Deltagerens poengsum er: " << prestasjon << endl;
  } else {
    cout << "Deltagerens tid er: " << prestasjon << endl;
  }
}

void Resultatdeltager::skrivTilFil(ofstream &utfil) {
  utfil << idnummer << ',' << prestasjon << '\n';
}

Resultatdeltager::~Resultatdeltager() {

}
