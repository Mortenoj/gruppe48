#include "Nasjoner.h"
#include "ListTool2B.h"
#include "Global.h"
#include <string>
#include <cstring>
#include <iostream>
#include <fstream>
#include "Nasjon.h"

using namespace std;


Nasjoner::Nasjoner(){  // Konstruktor
  nasjonListe = new List(Sorted);


  ifstream innfilNasjoner("data/NASJONER.DTA");
  if(innfilNasjoner){
    string nasjonskode;
    string antNasjoner;
    getline(innfilNasjoner, antNasjoner, '\n');
    for (size_t i = 0; i < stoi(antNasjoner); i++) {
      getline(innfilNasjoner, nasjonskode, ',');
      nasjonListe->add(new Nasjon(innfilNasjoner, nasjonskode.c_str()));
    }
  } else {
    cout << "finner ikke fila Nasjoner.dta\n";
  }
}

void Nasjoner::skrivMeny() { // Menyvalg for nasjoner
  char kommando = '0';

  while (kommando != 'X') {
    cout << "\n\nFLGENDE KOMMANDOER ER TILGJENGELIGE:"
         << "\n\tN - Registrer ny nasjon"
         << "\n\tE - Endre en nasjon"
         << "\n\tA - Hoveddata om alle nasjoner"
         << "\n\tT - En nasjons deltager tropp"
         << "\n\tL - Startliste"
         << "\n\tS - All data om en nasjon"
         << "\n\tX - Tilbake";

         kommando = les();

      switch (kommando) {
      case 'N': regNasjon();          break;
      case 'E': endreNasjon();        break;
      case 'A': displayNasjoner();    break;
      case 'T': displayDeltagere();   break;
      case 'L': /*Innhold*/;          break;
      case 'S': displayNasjon();      break;
      default:                        break;
    }
  }
}

void Nasjoner::regNasjon() {
  Nasjon *nasjon;

  cout << "skriv inn en tresifret nasjonskode: ";

  char buffer[STRLEN];
  cin.getline(buffer, STRLEN);
  navn = new char[strlen(buffer) + 1];
  strcpy(navn, buffer);

  nasjon = new Nasjon(navn);
  nasjonListe->add(nasjon);
  nasjon = nullptr;

  skrivTilFil();
}
void Nasjoner::endreNasjon() {
  Nasjon *nasjon;

  cout << "Skriv inn nasjonskoden du vil endre data hos: ";

  char buffer[STRLEN];
  cin.getline(buffer, STRLEN);
  navn = new char[strlen(buffer) + 1];
  strcpy(navn, buffer);

  if(nasjonListe->inList(navn)) {
    nasjon = (Nasjon*)nasjonListe->remove(navn);
    nasjon->endreData();
    nasjonListe->add(nasjon);
  } else {
    cout << "Nasjon med kode " << navn << " finnes ikke!\n";
  }

  nasjon = nullptr;
}
bool Nasjoner::finnesNasjon (char* t) {
  return nasjonListe->inList(t);
}

void Nasjoner::displayNasjoner() {
  cout << "Antall nasjoner: " << nasjonListe->noOfElements() << endl;
  nasjonListe->displayList();

}

void Nasjoner::displayDeltagere() {
  deltagere.displayNasjonsDeltagere();

}

void Nasjoner::displayNasjon() {
  Nasjon* nasjon;
  cout << "Skriv inn nasjonskoden du vil se informasjon hos: ";
  char* nasjonskode;

  char buffer[STRLEN];
  cin.getline(buffer, STRLEN);
  nasjonskode = new char[strlen(buffer) + 1];
  strcpy(nasjonskode, buffer);

  if(nasjonListe->inList(nasjonskode)) {
    nasjon = (Nasjon*)nasjonListe->remove(nasjonskode);
    nasjon->display();
    nasjonListe->add(nasjon);
  } else {
    cout << "Nasjon med kode " << nasjonskode << " finnes ikke!\n";
  }
  nasjon = nullptr;
}
void Nasjoner::skrivTilFil() {
  Nasjon* nasjon;
  ofstream utfil("data/NASJONER.DTA", ios::trunc);
  utfil << nasjonListe->noOfElements() << "\n";
  for (int i = 1; i <= nasjonListe->noOfElements(); i++){
    nasjon = (Nasjon*)nasjonListe->removeNo(i);
    nasjon->skrivTilFil(utfil);
    nasjonListe->add(nasjon);
  }
}


Nasjoner::~Nasjoner() {  // Frigjør plass i minnet
  delete[] navn;
}
