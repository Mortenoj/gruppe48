#include "Gren.h"
#include "Global.h"
#include "ListTool2B.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include "Ovelse.h"

using namespace std;

Gren::Gren(char* n) : TextElement(n) {
  ovelsesListe = new List(Sorted);
  cout << "gren klasse er lagd";
  ovelser = 0;
  grenNavn = new char[strlen(n)+1];
  strcpy(grenNavn, n);
  registrerData();
}

Gren::Gren(ifstream &innfilGrener, const char* navn) : TextElement(navn) {
  ovelsesListe = new List(Sorted);

  string tmp;
  string id;
  string antOvelser;
  grenNavn = new char[strlen(navn) + 1];
  strcpy(grenNavn, navn);

  getline(innfilGrener, tmp, '\n');
  poengSystem = tmp[0];

  getline(innfilGrener, antOvelser, '\n');
    ovelser = stoi(antOvelser);

  for (size_t i = 0; i < ovelser; i++) {
    getline(innfilGrener, id, ',');
    ovelsesListe->add(new Ovelse(innfilGrener, id.c_str()));
  }
}

char Gren::hentPoengSystem() {
  return poengSystem;
}

void Gren::skrivTilFil(ofstream &utfil) {
  utfil << grenNavn << ","
        << poengSystem << "\n";

  utfil << ovelser << "\n";
  for (size_t i = 1; i <= ovelsesListe->noOfElements(); i++) {
      Ovelse* ovelse = (Ovelse*)ovelsesListe->removeNo(i);
      ovelsesListe->add(ovelse);
      ovelse->skrivTilFil(utfil);
  }
}

void Gren::registrerData() {
  cout << "\nSkriv inn grenens poengsystem\n\n"
      << "FÂLGENDE KOMMANDOER ER TILGJENGELIGE: \n\tP - Poeng\n\tT - tid\n";
  poengSystem = les();
}
void Gren::endreData() {
  char* nyttNavn;
  char buffer[STRLEN];

  cout << "Skriv inn grenens nye navn: ";
  cin.getline(buffer, STRLEN);
  nyttNavn = new char[strlen(buffer) + 1];
  strcpy(nyttNavn, buffer);

  if(grener.finnesGren(nyttNavn)) {
    cout << "Denne grenen finnes fra fÃ¸r!" << endl;
  } else {
    grenNavn = new char[strlen(buffer) + 1];
    strcpy(grenNavn, buffer);
    grener.skrivTilFil();
  }
}


void Gren::lesFraFil() {

}
void Gren::display() {
  string strPoengSystem;
  switch (poengSystem) {
    case 'P': strPoengSystem = "Poeng";   break;
    case 'T': strPoengSystem = "Tid";     break;
  }
  cout << endl << "Grenens navn er: " << grenNavn
      << "\nGrenens poengsystem er " << strPoengSystem
      << "\nAntall Øvelser i " << grenNavn << " er: " << ovelser << "\n\n";
}
void Gren::displayGren() {
  string strPoengSystem;
  switch (poengSystem) {
    case 'P': strPoengSystem = "Poeng";   break;
    case 'T': strPoengSystem = "Tid";     break;
  }
  cout << endl << "Grenens navn er: " << grenNavn
      << "\nGrenens poengsystem er " << strPoengSystem
      << "\nAntall Øvelser i " << grenNavn << " er: " << ovelser << "\n\n"
      << "Øvelser:\n";
      ovelsesListe->displayList();
}

void Gren::skrivOvelsemeny(char* grenNavn) {

  char kommando = '0';

   while (kommando != 'X') {
     cout << "\n\nFÂLGENDE KOMMANDOER ER TILGJENGELIGE:"
          << "\n\tN - Registrer ny ovelse"
          << "\n\tE - Endre en ovelse"
          << "\n\tF - Fjerne/slette en ovelse"
          << "\n\tA - Skriv hoveddata om alle ovelser"
          << "\n\tL - Startliste"
          << "\n\tR - Resultatliste"
          << "\n\tX - Tilbake";

      kommando = les();

     switch (kommando) {
     case 'N': regNyOvelse();                         break;
     case 'E': endreOvelse();                         break;
     case 'F': fjernOvelse();                         break;
     case 'A': displayGren();                         break;
     case 'L': startListeMeny();                      break;
     case 'R': resultatListeMeny(grenNavn);           break;
     default:                                         break;
    }
  }
}

void Gren::regNyOvelse() {
  char* ovelsesID{};
  char buffer[STRLEN]{};

  cout << "Skriv inn den nye ovelsens unike ID: ";
  cin.getline(buffer, STRLEN);
  ovelsesID = new char[strlen(buffer) + 1];
  strcpy(ovelsesID, buffer);

  if(ovelsesListe->inList(ovelsesID)){
    cout << "ovelse med ID " << ovelsesID << " i grenen "
          << grenNavn << " finnes allerede";
  } else {
    ovelsesListe->add(new Ovelse(ovelsesID));
    ovelser++;
    grener.skrivTilFil();
  }
}

void Gren::endreOvelse() {
  Ovelse* ovelse;
  char* ovelsesID{};
  char buffer[STRLEN]{};
  cout << "skriv inn IDen til ovelsen du vil endre data hos: ";
  cin.getline(buffer, STRLEN);
  ovelsesID = new char[strlen(buffer) + 1];
  strcpy(ovelsesID, buffer);

  if(ovelsesListe->inList(ovelsesID)){
    ovelse = (Ovelse*)ovelsesListe->remove(ovelsesID);
    ovelse->endreData();
    ovelsesListe->add(ovelse);
    grener.skrivTilFil();
  } else {
    cout << "en ovelse med ID: " << ovelsesID << " i grenen: " << grenNavn << " finnes ikke";
  }
}

void Gren::fjernOvelse(){ //NBNB funksjonen mÃ¥ ogsÃ¥ fjerne filer assosiert med ovelsen +
  Ovelse* ovelse;         //oppdatere medalje + poengstatistikken!!!!
  char* ovelsesID{};
  char buffer[STRLEN]{};
  cout << "skriv inn IDen til ovelsen du vil fjerne: ";
  cin.getline(buffer, STRLEN);
  ovelsesID = new char[strlen(buffer) + 1];
  strcpy(ovelsesID, buffer);

  if(ovelsesListe->inList(ovelsesID)){
    ovelse = (Ovelse*)ovelsesListe->remove(ovelsesID);
    ofstream utfil("data/GRENER.DTA", ios::trunc);
    grener.skrivTilFil();
  } else {
    cout << "en ovelse med ID: " << ovelsesID << " i grenen: " << grenNavn << " finnes ikke";
  }

}
void Gren::startListeMeny() {
  Ovelse* ovelse;
  string ovelsesID;
  cout << "skriv inn IDen til ovelsen du vil endre data hos: ";
  cin >> ovelsesID;

  if(ovelsesListe->inList(ovelsesID.c_str())){
    ovelse = (Ovelse*)ovelsesListe->remove(ovelsesID.c_str());
    ovelse->skrivStartListeMeny((char*)ovelsesID.c_str());
    ovelsesListe->add(ovelse);
  } else {
    cout << "en ovelse med ID: " << ovelsesID << " i grenen: " << grenNavn << " finnes ikke";
  }

}

void Gren::resultatListeMeny(char* grenNavn) {
  Ovelse* ovelse;
  char* ovelsesID{};
  char buffer[STRLEN]{};
  cout << "skriv inn IDen til ovelsen du vil endre data hos: ";
  cin.getline(buffer, STRLEN);
  ovelsesID = new char[strlen(buffer) + 1];
  strcpy(ovelsesID, buffer);

  if(ovelsesListe->inList(ovelsesID)){
    ovelse = (Ovelse*)ovelsesListe->remove(ovelsesID);
    ovelsesListe->add(ovelse);
    ovelse->skrivResultatListeMeny(ovelsesID, grenNavn);
  } else {
    cout << "en ovelse med ID: " << ovelsesID << " i grenen: " << grenNavn << " finnes ikke";
  }


}

Gren::~Gren() {
  delete ovelsesListe;

}
