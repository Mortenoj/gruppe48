#include "Deltager.h"
#include <iostream>
#include "ListTool2B.h"
#include <cstring>
#include <fstream>
#include <string>
#include "Global.h"


using namespace std;


Deltager::Deltager(int id, char* t) : NumElement(id) {//konstruktor med parameter
  deltagerID = id;
  forkortelse = new char[4];
  strcpy(forkortelse, t);
  registrerData();
}

Deltager::Deltager(ifstream &innfilDeltagere, const int id) : NumElement(id) {
  deltagerID = id;
  forkortelse = new char[4];
  string forkortelseTmp;
  string kjonnTmp;

  getline(innfilDeltagere, navn, ',');

  getline(innfilDeltagere, forkortelseTmp, ',');
  strcpy(forkortelse, forkortelseTmp.c_str());

  getline(innfilDeltagere, kjonnTmp, '\n');
  if(kjonnTmp == "mann") {
    kjonn = false;
  } else if(kjonnTmp == "dame") {
    kjonn = true;
  } else {
    cout << "Ugyldig kjønn!" << endl;
  }
}

void Deltager::registrerData() {
  string kjonnstr;

  if(nasjoner.finnesNasjon(forkortelse)){
    cout << "\nSkriv inn data for nasjonen med forkortelse: " << forkortelse <<"\n\n";
    cout << "skriv inn deltagerens navn: ";
    cin >> navn;

    cout << "Skriv inn deltagerens kjonn (mann/dame): ";
    do {
      cin >> kjonnstr;
      if(kjonnstr == "mann"){
        kjonn = false;
      }else if(kjonnstr == "dame"){
        kjonn = true;
      }else {
        cout << "tilatte valg: mann/dame: ";
      }
    } while(kjonnstr!= "mann" && kjonnstr!= "dame");
  } else {
    cout << "Nasjon med forkortelese " << forkortelse <<" finnes ikke!\n";
  }
}
void Deltager::endreData() {
  registrerData();
}
void Deltager::display() {  // Printer ut data om nasjon
  cout << endl << "Deltagerens unike ID " << deltagerID
       << endl << "Deltagerens navn: " << navn << endl
       << "Deltagerens hjemland: " << forkortelse << endl
       << "Deltagerens kjonn er: ";
  if(kjonn == false){
    cout << "mann\n";
  } else {
    cout << "dame\n";
  }
}
string Deltager::hentLand() {
  string land(forkortelse);
  return land;
}

void Deltager::skrivTilFil(ofstream &utfil) {
  utfil << deltagerID << ','
        << navn << ','
        << forkortelse << ',';
  if(kjonn == 1) {
    utfil << "dame\n";
  } else {
    utfil << "mann\n";
  }
}

void Deltager::lesFraFil() {

}

string Deltager::hentNavn() {
  return navn;
}

int Deltager::hentID() {
  return number;
}

Deltager::~Deltager() {

}
