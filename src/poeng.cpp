#include "Poeng.h"
#include "ListTool2B.h"
#include <cstring>
#include <string>
#include <iostream>
#include <fstream>
#include "Global.h"

using namespace std;

Poeng::Poeng() {
  for (size_t i = 0; i < MAXKORTNAVN; i++) {
    for (size_t j = 0; j < 2; j++) {
      poengOversikt[j][i] = "0";
    }
  }
}

void Poeng::oppdater(string resfil, bool poeng) {
  for (int i = 0; i < MAXKORTNAVN; i++) {
    poengOversikt[0][i] = nasjoner[i];
  }

  ifstream innfil(resfil);
  if(innfil.good()) {
    string antDeltagere = "0";
    string tmp = "0";
    string navn{};
    string prest = "0";
    string enNavn{};
    string toNavn{};
    string treNavn{};
    string fireNavn{};
    string femNavn{};
    string seksNavn{};
    string enPoeng = "0";
    string toPoeng = "0";
    string trePoeng = "0";
    string firePoeng = "0";
    string femPoeng = "0";
    string seksPoeng = "0";
    getline(innfil, antDeltagere, '\n');
    if(poeng == true) {
      for(int i = 0; i < stoi(antDeltagere); i++) {
        getline(innfil, navn, ',');
        getline(innfil, prest, '\n');
        if(stoi(prest) > stoi(seksPoeng)) {
          seksNavn = navn;
          seksPoeng = prest;
          if(stoi(prest) > stoi(femPoeng)) {
            seksNavn = femNavn;
            seksPoeng = femPoeng;
            femNavn = navn;
            femPoeng = prest;
            if(stoi(prest) > stoi(firePoeng)) {
              femNavn = fireNavn;
              femPoeng = firePoeng;
              fireNavn = navn;
              firePoeng = prest;
              if(stoi(prest) > stoi(trePoeng)) {
                fireNavn = treNavn;
                firePoeng = trePoeng;
                treNavn = navn;
                trePoeng = prest;
                if(stoi(prest) > stoi(toPoeng)) {
                  treNavn = toNavn;
                  trePoeng = toPoeng;
                  toNavn = navn;
                  toPoeng = prest;
                  if(stoi(prest) > stoi(enPoeng)) {
                    toNavn = enNavn;
                    toPoeng = enPoeng;
                    enNavn = navn;
                    enPoeng = prest;
                  }
                }
              }
            }
          }
        }
      }
      for(int i = 0; i < sistebrukte; i++) {
        if(deltagere.hentNasjon(stoi(enNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp += 7;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(toNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp += 5;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(treNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp += 4;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(fireNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp += 3;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(femNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp += 2;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(seksNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp += 1;
          poengOversikt[1][i] = to_string(tmp);
        }
      }
    } else {
      enPoeng = "99999999"; // Høyest mulig verdi
      toPoeng = "99999999";
      trePoeng = "99999999";
      firePoeng = "99999999";
      femPoeng = "99999999";
      seksPoeng = "99999999";

      for(int i = 0; i < stoi(antDeltagere); i++) {
        getline(innfil, navn, ',');
        getline(innfil, prest, '\n');
        if(stoi(prest) < stoi(seksPoeng)) {
          seksNavn = navn;
          seksPoeng = prest;
          if(stoi(prest) < stoi(femPoeng)) {
            seksNavn = femNavn;
            seksPoeng = femPoeng;
            femNavn = navn;
            femPoeng = prest;
            if(stoi(prest) < stoi(firePoeng)) {
              femNavn = fireNavn;
              femPoeng = firePoeng;
              fireNavn = navn;
              firePoeng = prest;
              if(stoi(prest) < stoi(trePoeng)) {
                fireNavn = treNavn;
                firePoeng = trePoeng;
                treNavn = navn;
                trePoeng = prest;
                if(stoi(prest) < stoi(toPoeng)) {
                  treNavn = toNavn;
                  trePoeng = toPoeng;
                  toNavn = navn;
                  toPoeng = prest;
                  if(stoi(prest) < stoi(enPoeng)) {
                    toNavn = enNavn;
                    toPoeng = enPoeng;
                    enNavn = navn;
                    enPoeng = prest;
                  }
                }
              }
            }
          }
        }
      }
      for(int i = 0; i < sistebrukte; i++) {
        if(deltagere.hentNasjon(stoi(enNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp += 7;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(toNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp += 5;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(treNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp += 4;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(fireNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp += 3;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(femNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp += 2;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(seksNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp += 1;
          poengOversikt[1][i] = to_string(tmp);
        }
      }
    }
  }
  innfil.close();
  ofstream utfil("data/poeng.DTA", ios::trunc);
  for (size_t i = 0; i < sistebrukte; i++) {
    for (size_t j = 0; j < 2; j++) {
      utfil << poengOversikt[j][i] << ",";
    }
    utfil << "\n";
  }
  utfil.close();
}

void Poeng::oppdaterSlett(string resfil, bool poeng) {
  for (int i = 0; i < MAXKORTNAVN; i++) {
    poengOversikt[0][i] = nasjoner[i];
  }

  ifstream innfil(resfil);
  if(innfil.good()) {
    string antDeltagere = "0";
    string tmp = "0";
    string navn{};
    string prest = "0";
    string enNavn{};
    string toNavn{};
    string treNavn{};
    string fireNavn{};
    string femNavn{};
    string seksNavn{};
    string enPoeng = "0";
    string toPoeng = "0";
    string trePoeng = "0";
    string firePoeng = "0";
    string femPoeng = "0";
    string seksPoeng = "0";
    getline(innfil, antDeltagere, '\n');
    if(poeng == true) {
      for(int i = 0; i < stoi(antDeltagere); i++) {
        getline(innfil, navn, ',');
        getline(innfil, prest, '\n');
        if(stoi(prest) > stoi(seksPoeng)) {
          seksNavn = navn;
          seksPoeng = prest;
          if(stoi(prest) > stoi(femPoeng)) {
            seksNavn = femNavn;
            seksPoeng = femPoeng;
            femNavn = navn;
            femPoeng = prest;
            if(stoi(prest) > stoi(firePoeng)) {
              femNavn = fireNavn;
              femPoeng = firePoeng;
              fireNavn = navn;
              firePoeng = prest;
              if(stoi(prest) > stoi(trePoeng)) {
                fireNavn = treNavn;
                firePoeng = trePoeng;
                treNavn = navn;
                trePoeng = prest;
                if(stoi(prest) > stoi(toPoeng)) {
                  treNavn = toNavn;
                  trePoeng = toPoeng;
                  toNavn = navn;
                  toPoeng = prest;
                  if(stoi(prest) > stoi(enPoeng)) {
                    toNavn = enNavn;
                    toPoeng = enPoeng;
                    enNavn = navn;
                    enPoeng = prest;
                  }
                }
              }
            }
          }
        }
      }
      for(int i = 0; i < sistebrukte; i++) {
        if(deltagere.hentNasjon(stoi(enNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp -= 7;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(toNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp -= 5;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(treNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp -= 4;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(fireNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp -= 3;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(femNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp -= 2;
          poengOversikt[1][i] = to_string(tmp);
        } if (deltagere.hentNasjon(stoi(seksNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp -= 1;
          poengOversikt[1][i] = to_string(tmp);
        }
      }
    } else {
      enPoeng = "99999999"; // Høyest mulig verdi
      toPoeng = "99999999";
      trePoeng = "99999999";
      firePoeng = "99999999";
      femPoeng = "99999999";
      seksPoeng = "99999999";

      for(int i = 0; i < stoi(antDeltagere); i++) {
        getline(innfil, navn, ',');
        getline(innfil, prest, '\n');
        if(stoi(prest) < stoi(seksPoeng)) {
          seksNavn = navn;
          seksPoeng = prest;
          if(stoi(prest) < stoi(femPoeng)) {
            seksNavn = femNavn;
            seksPoeng = femPoeng;
            femNavn = navn;
            femPoeng = prest;
            if(stoi(prest) < stoi(firePoeng)) {
              femNavn = fireNavn;
              femPoeng = firePoeng;
              fireNavn = navn;
              firePoeng = prest;
              if(stoi(prest) < stoi(trePoeng)) {
                fireNavn = treNavn;
                firePoeng = trePoeng;
                treNavn = navn;
                trePoeng = prest;
                if(stoi(prest) < stoi(toPoeng)) {
                  treNavn = toNavn;
                  trePoeng = toPoeng;
                  toNavn = navn;
                  toPoeng = prest;
                  if(stoi(prest) < stoi(enPoeng)) {
                    toNavn = enNavn;
                    toPoeng = enPoeng;
                    enNavn = navn;
                    enPoeng = prest;
                  }
                }
              }
            }
          }
        }
      }
      for(int i = 0; i < sistebrukte; i++) {
        if(deltagere.hentNasjon(stoi(enNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp -= 7;
          poengOversikt[1][i] = to_string(tmp);
        }  if (deltagere.hentNasjon(stoi(toNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp -= 5;
          poengOversikt[1][i] = to_string(tmp);
        }  if (deltagere.hentNasjon(stoi(treNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp -= 4;
          poengOversikt[1][i] = to_string(tmp);
        }  if (deltagere.hentNasjon(stoi(fireNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp -= 3;
          poengOversikt[1][i] = to_string(tmp);
        }  if (deltagere.hentNasjon(stoi(femNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp -= 2;
          poengOversikt[1][i] = to_string(tmp);
        }  if (deltagere.hentNasjon(stoi(seksNavn)) == nasjoner[i]) {
          int tmp = stoi(poengOversikt[1][i]);
          tmp -= 1;
          poengOversikt[1][i] = to_string(tmp);
        }
      }
    }
  }
  innfil.close();
  ofstream utfil("data/poeng.DTA", ios::trunc);
  for (size_t i = 0; i < sistebrukte; i++) {
    for (size_t j = 0; j < 2; j++) {
      utfil << poengOversikt[j][i] << ",";
    }
    utfil << "\n";
  }
  utfil.close();
}


void Poeng::display() {
  ifstream innfil("data/poeng.DTA");
  string tmp;
  for (size_t i = 0; i < sistebrukte; i++) {
    for (size_t j = 0; j < 2; j++) {
      getline(innfil, tmp, ',');
      cout << tmp;
    }
    getline(innfil, tmp, '\n');
    cout << endl;
  }
  innfil.close();
}

Poeng::~Poeng() {

}
