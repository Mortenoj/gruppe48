#include "Statistikk.h"
#include <iostream>
#include <fstream>
#include <string>
#include "Global.h"
#include <cstring>

using namespace std;

Statistikk::Statistikk() {
  ifstream innfilNasjoner("data/NASJONER.DTA");
  string antNasjoner, tmp;
  if(innfilNasjoner) {
    getline(innfilNasjoner, antNasjoner, '\n');
    for (int i = 0; i < stoi(antNasjoner); i++) {
      getline(innfilNasjoner, nasjoner[i], ',');
      for (int i = 0; i < 3; i++) {
        getline(innfilNasjoner, tmp, ',');
      }
      getline(innfilNasjoner, tmp, '\n');
    }
  }
  sistebrukte = stoi(antNasjoner);
}

Statistikk::~Statistikk() {

}
