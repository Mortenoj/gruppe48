#include "Deltagere.h"
#include "Global.h"
#include <iostream>
#include <fstream>
#include "ListTool2B.h"
#include <string>
#include <cstring>

using namespace std;


Deltagere::Deltagere() {
  deltagerListe = new List(Sorted);

  ifstream innfilDeltagere("data/DELTAGERE.DTA");
  if(innfilDeltagere) {
    string deltagerID;
    string antDeltagere;
    getline(innfilDeltagere, antDeltagere, '\n');
    for (size_t i = 0; i < stoi(antDeltagere); i++) {
      getline(innfilDeltagere, deltagerID, ',');
      deltagerListe->add(new Deltager(innfilDeltagere, stoi(deltagerID)));
    }
  } else {
    cout << "Finner ikke filen Deltagere.dta" << endl;
  }
}

void Deltagere::skrivMeny() {
  char kommando = '0';
   while (kommando != 'X') {
     cout << "\n\nFLGENDE KOMMANDOER ER TILGJENGELIGE:"
     << "\n\tN - Registrer ny deltager"
     << "\n\tE - Endre en deltager"
     << "\n\tA - Hoveddata om alle deltagere"
     << "\n\tS - All data om en deltager"
     << "\n\tX - Tilbake";

     kommando = les();

     switch (kommando) {
     case 'N': regDeltager();            break;
     case 'E': endreDeltager();          break;
     case 'A': displayDeltagere();       break;
     case 'S': displayEnkeltDeltager();  break;
     default:                            break;
    }
  }
}

void Deltagere::regDeltager() {
  int id;

  cout << "Skriv inn delagerens unike ID: ";
  cin >> id;

  cout << "Skriv inn deltagerens tresifrede nasjonskode: ";

  char landskode[4];
  cin >> landskode;
  if (deltagerListe->inList(id)) {
    cout << "IDen finnes fra før av" << '\n';
  } else {
    if (nasjoner.finnesNasjon(landskode)) {
      deltagerListe->add(new Deltager(id, landskode));
    } else {
      cout << "IDen finnes ikke fra før av, men ingen land med kode " << landskode << "\n";
    }
  }
  skrivTilFil();
}

void Deltagere::endreDeltager() {
  int id;
  Deltager *deltager;

  cout << "Skriv inn delagerens unike ID: ";
  cin >> id;

  if(!deltagerListe->inList(id)) {
    cout << "Deltager med id: " << id << " eksisterer ikke!" << endl;
  } else {
    cout << "Endre på deltager med id: " << id << ": " << endl;

    deltager = (Deltager*)deltagerListe->remove(id);
    deltager->endreData();
    deltagerListe->add(deltager);
  }

  deltager = nullptr;
}
void Deltagere::displayDeltagere() {
  cout << "Antall Deltagere: " << deltagerListe->noOfElements() << endl;
  deltagerListe->displayList();
}
void Deltagere::displayNasjonsDeltagere() {
  char* nasjonskode;
  cout << "skriv inn en tresifret nasjonskode: ";
  char buffer[STRLEN];
  cin.getline(buffer, STRLEN);
  nasjonskode = new char[strlen(buffer) + 1];
  strcpy(nasjonskode, buffer);

  Deltager* deltager;
  for (size_t i = 0; i < deltagerListe->noOfElements(); i++) {
    deltager = (Deltager*)deltagerListe->removeNo(i);
    if(strcmp((deltager->hentLand().c_str()), nasjonskode) == 0) {
      deltager->display();
    }
    deltagerListe->add(deltager);
  }
}

void Deltagere::displayEnkeltDeltager() {
  int id;
  string deltagerNavn;

  cout << "Skriv inn deltagers ID-nummer: ";
  cin >> id;

  if(deltagerListe->inList(id)) {
    deltagerListe->displayElement(id);
  } else {
    cout << "Deltager med id: " << id << " eksisterer ikke!" << endl
         << "Skriv inn navnet til deltager: ";
    cin >> deltagerNavn;

    Deltager* tmp;
    for (size_t i = 1; i <= deltagerListe->noOfElements(); i++) {
      tmp = (Deltager*)deltagerListe->removeNo(i);
      string navn = tmp->hentNavn();
      if(deltagerNavn.compare(navn) == 0) {
        tmp->display();
      } else {
        cout << "Finner ikke deltager med navn: " << deltagerNavn << endl;
      }
      deltagerListe->add(tmp);
    }
    tmp = nullptr;
  }
}

void Deltagere::slettEnDeltager(int nr) {
  deltagerListe->removeNo(nr);
}

void Deltagere::skrivTilFil() {
  Deltager* deltager;
  ofstream utfil("data/DELTAGERE.DTA", ios::trunc);
  if(utfil) {
    utfil << deltagerListe->noOfElements() << "\n";
    for (size_t i = 1; i <= deltagerListe->noOfElements(); i++) {
      deltager = (Deltager*)deltagerListe->removeNo(i);
      deltager->skrivTilFil(utfil);
      deltagerListe->add(deltager);
    }
  } else {
    cout << "Kunne ikke lage fil DETLAGERE.DTA!" << endl;
  }
}

bool Deltagere::finnesDeltager(int deltagerID) {
  return(deltagerListe->inList(deltagerID));

}
string Deltagere::hentNavn(int deltagerID) {
  Deltager* deltager = (Deltager*)deltagerListe->remove(deltagerID);
  deltagerListe->add(deltager);
  return deltager->hentNavn();
}

string Deltagere::hentNasjon(int deltagerID) {
  Deltager* deltager = (Deltager*)deltagerListe->remove(deltagerID);
  deltagerListe->add(deltager);
  return deltager->hentLand();

}




Deltagere::~Deltagere() {
 delete deltagerListe;

}
