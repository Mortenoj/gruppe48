#include "Ovelse.h"
#include <iostream>
#include <fstream>
#include "Global.h"
#include <string>
#include "ListTool2B.h"
#include <stdio.h>
#include "Startdeltager.h"
#include "Resultatdeltager.h"

using namespace std;


Ovelse::Ovelse(char* n) : TextElement(n) {
  startListe = new List(Sorted);
  resultatListe = new List(Sorted);
  regData();
}

Ovelse::Ovelse(ifstream &innfilGrener, const char* ovelsesID) : TextElement(ovelsesID) {
  startListe = new List(Sorted);
  resultatListe = new List(Sorted);

  string date;
  string tid;
  string antallDeltagere;

  ovelseNavn = string(ovelsesID);

  getline(innfilGrener, ovelseNavn, ',');
  getline(innfilGrener, date, ',');
  getline(innfilGrener, tid, '\n');

  dato = stoi(date);
  klokkeslett = stoi(tid);


  string startlisteFil = "data/startlister/";
  startlisteFil.append(ovelsesID);
  startlisteFil.append(".STA");
  string antDeltagere;
  string tmp;

  ifstream innfils(startlisteFil);
  if(innfils.good()) {
    getline(innfils, antDeltagere, '\n');
    for (size_t i = 0; i < stoi(antDeltagere); i++) {
      getline(innfils, tmp, ',');
      startListe->add(new Startdeltager(innfils, stoi(tmp)));
    }
    innfils.close();
  }



  string resultatListeFil = "data/resultatlister/";
  resultatListeFil.append(ovelsesID);
  resultatListeFil.append(".RES");

  ifstream innfilr(resultatListeFil);
  if(innfilr.good()) {
    getline(innfilr, antDeltagere, '\n');
    for (size_t i = 0; i < stoi(antDeltagere); i++) {
      getline(innfilr, tmp, ',');
      resultatListe->add(new Resultatdeltager(innfilr, stoi(tmp)));
    }
    innfilr.close();
  }
}

void Ovelse::regData() {
  cout << "skriv inn navnet paa ovelsen: ";
  cin >> ovelseNavn;
  cout << "Skriv inn datoen paa ovelsen(MMDD): ";
  cin >> dato;
  cout << "skriv inn klokkeslettet paa ovelsen(TTMM): ";
  cin >> klokkeslett;
}

void Ovelse::endreData() {
  /*Navn, dato og klokkeslett kan endres (ikke alt det andre).*/
  regData();
}

void Ovelse::display() {
  cout  << "\nOvelsens navn: " << ovelseNavn
        << "\nOvelsens dato: " << dato
        << "\nOvelsens klokkeslett: " << klokkeslett
        << "\nAntall deltagere: " << startListe->noOfElements() << "\n\n";
}



void Ovelse::skrivStartListeMeny(char* ovelsesID) {
  char kommando = '0';
  while (kommando != 'X') {
    cout << "\n\nFLGENDE KOMMANDOER ER TILGJENGELIGE:"
    << "\n\tS - Skriv deltager-/startliste"
    << "\n\tN - Registrer ny deltager-/startliste"
    << "\n\tE - Endre en deltager-/startliste"
    << "\n\tF - Slett en deltager-/startliste"
    << "\n\tX - Tilbake";

    kommando = les();

     switch (kommando) {
     case 'S': skrivStartListe(ovelsesID);                break;
     case 'N': nyDeltagerListe(ovelsesID);                break;
     case 'E': endreDeltagerListe(ovelsesID);             break;
     case 'F': slettDeltagerListe(ovelsesID);             break;
     default:                                             break;
   }
   skrivStartListeTilFil(ovelsesID);
  }
}

void Ovelse::skrivResultatListeMeny(char* ovelsesID, char* grenNavn) {
  char kommando = '0';


  while (kommando != 'X') {
    cout << "\n\nFLGENDE KOMMANDOER ER TILGJENGELIGE:"
    << "\n\tS - Skriv resultatliste"
    << "\n\tN - Ny resultatliste"
    << "\n\tF - Slett en resultatliste"
    << "\n\tX - Tilbake";

    kommando = les();

     switch (kommando) {
     case 'S': skrivResultatListe();                              break;
     case 'N': nyResultatListe(ovelsesID, grenNavn);              break;
     case 'F': slettResultatListe(ovelsesID, grenNavn);                     break;
     default:                                                     break;
    }
  }
}

void Ovelse::skrivResultatListe() {
  resultatListe->displayList();

}
void Ovelse::nyResultatListe(char* ovelsesID, char* grenNavn) {
  int prestasjon, tid, ps;
  string svar;
  char tidSys; //T-tidel H-hundredel M-milisekund
  Startdeltager* startdeltager;

  if(grener.hentPoengSystem(grenNavn) == 'P') {
    p = true;
  } else {
    p = false;
  }

  if(p == true) {
    cout << "vil du ha ett eller to desimaler i poengene (1/2)";

  } else {
    cout << "Vil du registrere tid med tideler (MMSST), hundredeler (MMSSHH) eller tusendeler (MMSSTTT)\n"
          << "tideler: 3\nhundredeler: 4\n tusendeler: 5: ";
  }
  cin >> ps;

  for (size_t i = 1; i <= startListe->noOfElements(); i++) {
    startdeltager = (Startdeltager*)startListe->removeNo(i);
    string deltagerID = startdeltager->hentId();

      cout  << "Skriv inn resultatet for " << deltagere.hentNavn(stoi(deltagerID)) << "("
            << deltagerID << ")\n";

      if(ps == 1) {
        do {
          cout << "skriv inn tid i valgt format(PPT): ";
          cin >> svar;
        } while(stoi(svar) > 1000);
      } else if (ps == 2) {
        do {
          cout << "skriv inn tid i valgt format(PPHH): ";
          cin >> svar;
        } while(stoi(svar) > 10000);
      } else if(ps == 3) {
        do {
          cout << "skriv inn tid i valgt format(MMSST): ";
          cin >> svar;
        } while(stoi(svar) > 100000);
      } else if(ps == 4) {
        do {
          cout << "skriv inn tid i valgt format(MMSSHH): ";
          cin >> svar;
        } while(stoi(svar) > 1000000);
      } else if(ps == 5) {
        do {
          cout << "skriv inn tid i valgt format(MMSSTTT): ";
          cin >> svar;
        } while(stoi(svar) > 10000000);
      }
      resultatListe->add(new Resultatdeltager(stoi(svar), stoi(deltagerID), p));
      startListe->add(startdeltager);
    }
    skrivResultatListeTilFil(ovelsesID, p);
}

void Ovelse::skrivResultatListeTilFil(char* ovelsesID, bool po) {
  Resultatdeltager* resultatDeltager;
  string resultatListeFil = "data/resultatlister/";
  resultatListeFil.append(ovelsesID);
  resultatListeFil.append(".RES");

  ofstream utfil(resultatListeFil, ios::trunc);
  if(utfil.good())  {
    utfil << resultatListe->noOfElements() << "\n";
    for (int i = 1; i <= resultatListe->noOfElements(); i++){
      resultatDeltager = (Resultatdeltager*)resultatListe->removeNo(i);
      resultatListe->add(resultatDeltager);
      resultatDeltager->skrivTilFil(utfil);
    }
  }
  utfil.close();
  medaljer.oppdater(resultatListeFil, po);
  poeng.oppdater(resultatListeFil, po);
}

void Ovelse::slettResultatListe(char* ovelsesID, char* grenNavn)  {
  string resultatListeFil = "data/resultatlister/";
  resultatListeFil.append(ovelsesID);
  resultatListeFil.append(".RES");

  if(grener.hentPoengSystem(grenNavn) == 'P') {
    p = true;
  } else {
    p = false;
  }


  medaljer.oppdaterSlett(resultatListeFil, p);
  poeng.oppdaterSlett(resultatListeFil, p);


  remove(resultatListeFil.c_str());
  delete resultatListe;
  resultatListe = new List(Sorted);

}

void Ovelse::nyDeltagerListe(char* ovelsesID) {
  char kommando = '0';
  while (kommando != 'X') {
    cout << "\n\nFLGENDE KOMMANDOER ER TILGJENGELIGE:"
    << "\n\tN - Ny deltager"
    << "\n\tA - oversikt over alle deltagere"
    << "\n\tX - Ferdig med aa registrere deltagere";

    kommando = les();
     switch (kommando) {
     case 'N': nyDeltager();                        break;
     case 'A': deltagere.displayDeltagere();        break;
     default:                                       break;
    }
    skrivStartListeTilFil(ovelsesID);
  }
}

void Ovelse::nyDeltager() {
  int deltagerID;
  cout << "skriv inn deltagerens unike id: ";
  cin >> deltagerID;

  if(deltagere.finnesDeltager(deltagerID)) {
    if(startListe->inList(deltagerID)) {
      cout << "Denne deltageren er allerede registrert. Mente du E - Endre en deltager?\n";
    } else {
      startListe->add(new Startdeltager(deltagerID));
    }
  } else {
    cout << "finner ikke deltager med ID nummer: " << deltagerID;
  }
}

void Ovelse::skrivStartListeTilFil(char* ovelsesID) {
  Startdeltager* startdeltager;
  string startlisteFil = "data/startlister/";
  startlisteFil.append(ovelsesID);
  startlisteFil.append(".STA");

  ofstream utfil(startlisteFil, ios::trunc);
  utfil << startListe->noOfElements() << "\n";
  for (int i = 1; i <= startListe->noOfElements(); i++){
    startdeltager = (Startdeltager*)startListe->removeNo(i);
    startdeltager->skrivTilFil(utfil);
    startListe->add(startdeltager);
  }
}

void Ovelse::skrivStartListe(char* ovelsesID) {
  string startlisteFil = "data/startlister/";
  startlisteFil.append(ovelsesID);
  startlisteFil.append(".STA");
  string startnummer, idnummer, navn, nasjon, tmp;

  ifstream innfil(startlisteFil);
  getline(innfil, tmp, '\n');
  while(innfil.good()) {
    getline(innfil, startnummer, ',');
    getline(innfil, idnummer, ',');
    getline(innfil, navn, ',');
    getline(innfil, nasjon, '\n');

    if(!innfil.eof()) {
      cout  << "Deltagerens startnummer: " << startnummer << endl
      << "Deltagerens ID: " << idnummer << endl
      << "Deltagerens navn: " << navn << endl
      << "Deltagerens nasjon: " << nasjon << endl << endl;
    }
  }
}

void Ovelse::endreDeltagerListe(char* ovelsesID) {
  char kommando = '0';
  while (kommando != 'X') {
    cout << "\n\nFOLGENDE KOMMANDOER ER TILGJENGELIGE:"
    << "\n\tE - Endre en deltager"
    << "\n\tF - Fjern en deltager"
    << "\n\tA - oversikt over deltagere"
    << "\n\tX - Ferdig med aa endre deltagere";

    kommando = les();
     switch (kommando) {
     case 'E': endreDeltager();                         break;
     case 'F': fjernDeltager();                          break;
     case 'A': startListe->displayList();               break;
     default:                                           break;
    }
    skrivStartListeTilFil(ovelsesID);
  }
}

void Ovelse::endreDeltager() {
  int deltagerID;
  cout << "skriv inn deltagerens unike id: ";
  cin >> deltagerID;

  if(deltagere.finnesDeltager(deltagerID)) {
    if(startListe->inList(deltagerID)) {
      startListe->remove(deltagerID);
      startListe->add(new Startdeltager(deltagerID));
    } else {
      cout  << "deltageren " << deltagere.hentNavn(deltagerID) << " er ikke registrert i "
            << "ovelsen: " << ovelseNavn;
    }
  } else {
    cout << "finner ikke deltager med ID nummer: " << deltagerID;
  }
}
void Ovelse::fjernDeltager() {
  int deltagerID;
  cout << "skriv inn deltagerens unike id: ";
  cin >> deltagerID;

  if(deltagere.finnesDeltager(deltagerID)) {
    startListe->remove(deltagerID);
  } else {
    cout << "finner ikke deltager med ID nummer: " << deltagerID;
  }

}

void Ovelse::slettDeltagerListe(char* ovelsesID) {
  string startlisteFil = "data/startlister/";
  startlisteFil.append(ovelsesID);
  startlisteFil.append(".STA");

  remove(startlisteFil.c_str());
  delete startListe;
  startListe = new List(Sorted);
  if(startListe->isEmpty()) {
    cout << "startlisten er fjernet" << endl;
  } else {
    cout << "kunne ikke fjerne startliste" << endl;
  }
}

void Ovelse::skrivTilFil(ofstream &utfil) {
  utfil << text << ","
        << ovelseNavn << ","
        << dato << ","
        << klokkeslett << "\n";
}




void Ovelse::lesFraFil() {

}

Ovelse::~Ovelse() {

}
