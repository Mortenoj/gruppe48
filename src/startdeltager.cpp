#include "Startdeltager.h"
#include <iostream>
#include <fstream>
#include "Global.h"
#include <string>
#include "ListTool2B.h"
#include <stdio.h>

Startdeltager::Startdeltager(int deltagerID) : NumElement(deltagerID) {
  regData(deltagerID);
}

Startdeltager::Startdeltager(ifstream &innfil, int deltagerID) : NumElement(deltagerID) {
  idnummer = to_string(deltagerID);
  getline(innfil, startnummer, ',');
  getline(innfil, navn, ',');
  getline(innfil, nasjon, '\n');
}
void Startdeltager::display() {
  cout << endl  << "Deltagerens startnummer er: " << startnummer << endl
                << "Deltagerens unike ID: " << idnummer << endl
                << "Deltagerens navn: " << navn << endl
                << "Deltagerens hjemland: " << nasjon << endl;
}

void Startdeltager::skrivTilFil(ofstream &utfil) {
  utfil << idnummer << ',' << startnummer << ',' << navn
  << ',' << nasjon << '\n';
}

void Startdeltager::regData(int deltagerID) {
    navn = deltagere.hentNavn(deltagerID);
    nasjon = deltagere.hentNasjon(deltagerID);
    idnummer = to_string(deltagerID);
    cout << "skriv inn " << navn << " sitt startnummer: ";
    cin >> startnummer;
}
string Startdeltager::hentId() {
  return idnummer;
}

Startdeltager::~Startdeltager() {

}
