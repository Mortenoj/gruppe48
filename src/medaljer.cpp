#include "Medaljer.h"
#include "Global.h"
#include <cstring>
#include <string>
#include <iostream>

using namespace std;

Medaljer::Medaljer() {
  for (size_t i = 0; i < MAXKORTNAVN; i++) {
    for (size_t j = 0; j < 4; j++) {
      medaljeOversikt[j][i] = "0";
    }
  }

}

void Medaljer::oppdaterSlett(string resfil, bool poeng) {
  for (int i = 0; i < MAXKORTNAVN; i++) {
    medaljeOversikt[0][i] = nasjoner[i];
  }

  ifstream innfil(resfil);
  if (innfil.good()) {
    string antDeltagere = "0";
    string tmp = "0";
    string navn{};
    string prest = "0";
    string gullNavn{};
    string solvNavn{};
    string bronsjeNavn{};
    string gullPoeng = "0";
    string solvPoeng = "0";
    string bronsjePoeng = "0";
    getline(innfil, antDeltagere, '\n');
    if(poeng == true) {
      for(int i = 0; i < stoi(antDeltagere); i++) {
        getline(innfil, navn, ',');
        getline(innfil, prest, '\n');
        if(stoi(prest) > stoi(bronsjePoeng)) {
          bronsjeNavn = navn;
          bronsjePoeng = prest;
          if(stoi(prest) > stoi(solvPoeng)) {
            bronsjeNavn = solvNavn;
            bronsjePoeng = solvPoeng;
            solvNavn = navn;
            solvPoeng = prest;
            if(stoi(prest) > stoi(gullPoeng)) {
              solvNavn = gullNavn;
              solvPoeng = gullPoeng;
              gullNavn = navn;
              gullPoeng = prest;
            }
          }
        }
      }
      for (int i = 0; i < sistebrukte; i++) {
        if(deltagere.hentNasjon(stoi(gullNavn)) == nasjoner[i]) {
          int tmp = stoi(medaljeOversikt[1][i]);
          tmp--;
          medaljeOversikt[1][i] = to_string(tmp);
        } if(deltagere.hentNasjon(stoi(solvNavn)) == nasjoner[i]) {
          int tmp = stoi(medaljeOversikt[2][i]);
          tmp--;
          medaljeOversikt[2][i] = to_string(tmp);
        } if(deltagere.hentNasjon(stoi(bronsjeNavn)) == nasjoner[i]) {
          int tmp = stoi(medaljeOversikt[3][i]);
          tmp--;
          medaljeOversikt[3][i] = to_string(tmp);
        }
      }
    } else {
      bronsjePoeng = "99999999"; // Hoyeste mulig verdi
      solvPoeng = "99999999";
      gullPoeng = "99999999";

      for(int i = 0; i < stoi(antDeltagere); i++) {
        getline(innfil, navn, ',');
        getline(innfil, prest, '\n');
        if(stoi(prest) < stoi(bronsjePoeng)) {
          bronsjeNavn = navn;
          bronsjePoeng = prest;
          if(stoi(prest) < stoi(solvPoeng)) {
            bronsjeNavn = solvNavn;
            bronsjePoeng = solvPoeng;
            solvNavn = navn;
            solvPoeng = prest;
            if(stoi(prest) < stoi(gullPoeng)) {
              solvNavn = gullNavn;
              solvPoeng = gullPoeng;
              gullNavn = navn;
              gullPoeng = prest;
            }
          }
        }
      }
      for (int i = 0; i < sistebrukte; i++) {
        if(deltagere.hentNasjon(stoi(gullNavn)) == nasjoner[i]) {
          int tmp = stoi(medaljeOversikt[1][i]);
          tmp--;
          medaljeOversikt[1][i] = to_string(tmp);
        } if(deltagere.hentNasjon(stoi(solvNavn)) == nasjoner[i]) {
          int tmp = stoi(medaljeOversikt[2][i]);
          tmp--;
          medaljeOversikt[2][i] = to_string(tmp);
        } if(deltagere.hentNasjon(stoi(bronsjeNavn)) == nasjoner[i]) {
          int tmp = stoi(medaljeOversikt[3][i]);
          tmp--;
          medaljeOversikt[3][i] = to_string(tmp);
        }
      }
    }
  }
  innfil.close();
  ofstream utfil("data/medaljer.DTA", ios::trunc);
  for (size_t i = 0; i < sistebrukte; i++) {
    for (size_t j = 0; j < 4; j++) {
      utfil << medaljeOversikt[j][i] << ",";
    }
    utfil << "\n";
  }
  utfil.close();
}

void Medaljer::oppdater(string resfil, bool poeng) {
  for (int i = 0; i < MAXKORTNAVN; i++) {
    medaljeOversikt[0][i] = nasjoner[i];
  }

  ifstream innfil(resfil);
  if (innfil.good()) {
    string antDeltagere = "0";
    string tmp = "0";
    string navn{};
    string prest = "0";
    string gullNavn{};
    string solvNavn{};
    string bronsjeNavn{};
    string gullPoeng = "0";
    string solvPoeng = "0";
    string bronsjePoeng = "0";
    getline(innfil, antDeltagere, '\n');
    if(poeng == true) {
      for(int i = 0; i < stoi(antDeltagere); i++) {
        getline(innfil, navn, ',');
        getline(innfil, prest, '\n');
        if(stoi(prest) > stoi(bronsjePoeng)) {
          bronsjeNavn = navn;
          bronsjePoeng = prest;
          if(stoi(prest) > stoi(solvPoeng)) {
            bronsjeNavn = solvNavn;
            bronsjePoeng = solvPoeng;
            solvNavn = navn;
            solvPoeng = prest;
            if(stoi(prest) > stoi(gullPoeng)) {
              solvNavn = gullNavn;
              solvPoeng = gullPoeng;
              gullNavn = navn;
              gullPoeng = prest;
            }
          }
        }
      }
      for (int i = 0; i < sistebrukte; i++) {
        if(deltagere.hentNasjon(stoi(gullNavn)) == nasjoner[i]) {
          int tmp = stoi(medaljeOversikt[1][i]);
          tmp++;
          medaljeOversikt[1][i] = to_string(tmp);
        } if(deltagere.hentNasjon(stoi(solvNavn)) == nasjoner[i]) {
          int tmp = stoi(medaljeOversikt[2][i]);
          tmp++;
          medaljeOversikt[2][i] = to_string(tmp);
        } if(deltagere.hentNasjon(stoi(bronsjeNavn)) == nasjoner[i]) {
          int tmp = stoi(medaljeOversikt[3][i]);
          tmp++;
          medaljeOversikt[3][i] = to_string(tmp);
        }
      }
    } else {
      bronsjePoeng = "99999999"; // Hoyeste mulig verdi
      solvPoeng = "99999999";
      gullPoeng = "99999999";

      for(int i = 0; i < stoi(antDeltagere); i++) {
        getline(innfil, navn, ',');
        getline(innfil, prest, '\n');
        if(stoi(prest) < stoi(bronsjePoeng)) {
          bronsjeNavn = navn;
          bronsjePoeng = prest;
          if(stoi(prest) < stoi(solvPoeng)) {
            bronsjeNavn = solvNavn;
            bronsjePoeng = solvPoeng;
            solvNavn = navn;
            solvPoeng = prest;
            if(stoi(prest) < stoi(gullPoeng)) {
              solvNavn = gullNavn;
              solvPoeng = gullPoeng;
              gullNavn = navn;
              gullPoeng = prest;
            }
          }
        }
      }
      for (int i = 0; i < sistebrukte; i++) {
        if(deltagere.hentNasjon(stoi(gullNavn)) == nasjoner[i]) {
          int tmp = stoi(medaljeOversikt[1][i]);
          tmp++;
          medaljeOversikt[1][i] = to_string(tmp);
        } if(deltagere.hentNasjon(stoi(solvNavn)) == nasjoner[i]) {
          int tmp = stoi(medaljeOversikt[2][i]);
          tmp++;
          medaljeOversikt[2][i] = to_string(tmp);
        } if(deltagere.hentNasjon(stoi(bronsjeNavn)) == nasjoner[i]) {
          int tmp = stoi(medaljeOversikt[3][i]);
          tmp++;
          medaljeOversikt[3][i] = to_string(tmp);
        }
      }
    }
  }
  innfil.close();
  ofstream utfil("data/medaljer.DTA", ios::trunc);
  for (size_t i = 0; i < sistebrukte; i++) {
    for (size_t j = 0; j < 4; j++) {
      utfil << medaljeOversikt[j][i] << ",";
    }
    utfil << "\n";
  }
  utfil.close();
}

void Medaljer::display() {
  ifstream innfil("data/medaljer.DTA");
  string tmp;
  for (size_t i = 0; i < sistebrukte; i++) {
    for (size_t j = 0; j < 4; j++) {
      getline(innfil, tmp, ',');
      cout << tmp;
    }
    getline(innfil, tmp, '\n');
    cout << endl;
  }
  innfil.close();
}




Medaljer::~Medaljer() {

}
