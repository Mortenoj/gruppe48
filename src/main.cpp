#include <iostream>
#include "Global.h"
#include "Medaljer.h"
#include "Poeng.h"

using namespace std;

//kun globale objekter

int main() {
  char kommando = '0';


	while (kommando != 'X') {
    cout << "\n\nFLGENDE KOMMANDOER ER TILGJENGELIGE:"
    << "\n\tN - Nasjoner"
    << "\n\tD - Deltagere"
    << "\n\tG - Grener"
    << "\n\tO - Ovelser"
    << "\n\tM - Medalje oversikt"
    << "\n\tP - Poeng oversikt"
    << "\n\tX - Avslutt program";

    kommando = les();

		switch (kommando) {
		case 'N': nasjoner.skrivMeny();         break;
		case 'D': deltagere.skrivMeny();        break;
		case 'G': grener.skrivMeny();           break;
		case 'O': grener.finnesGren();          break;
    case 'M': medaljer.display();           break;
    case 'P': poeng.display();              break;
		default:                                break;
		}
	}
  return 0;
}
