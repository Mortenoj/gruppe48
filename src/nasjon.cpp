#include "Nasjon.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include "ListTool2B.h"
#include "Global.h"

using namespace std;


Nasjon::Nasjon(char* n) : TextElement(n) {
  forkortelse = new char[4];  // Initialiserer data om nasjon
  strcpy(forkortelse, n);
  registrerData();
}

Nasjon::Nasjon(ifstream &innfilNasjoner, const char* nasjonskode) : TextElement(nasjonskode) {
  string tmp;
  forkortelse = new char[strlen(nasjonskode) + 1];
  strcpy(forkortelse, nasjonskode);
  getline(innfilNasjoner, navn, ',');

  getline(innfilNasjoner, tmp, ',');
  antDeltagere = stoi(tmp);
  getline(innfilNasjoner, kontaktPerson, ',');
  getline(innfilNasjoner, tmp, '\n');

  kontaktTlf = stoi(tmp);
}

void Nasjon::registrerData() {  // Leser inn data fra bruker
  cout << "Skriv inn data om nasjonen: " << endl;
  cout << "Nasjonens hele navn: ";
  cin >> navn;  // Henter inn navn

  cout << "Antall deltagere fra nasjonen: ";
  cin >> antDeltagere;    // Henter inn antall deltagere

  cout << "Navnet til kontaktperson: ";
  cin >> kontaktPerson;   // Henter inn navn på kontaktperson

  cout << "Tlf nr. til kontaktperson: ";
  cin >> kontaktTlf;    // Henter kontaktpersons tlf nr

  display();  //DEBUG

}

void Nasjon::endreData() {
  cout << "Skriv inn data om nasjonen: " << endl;
  cout << "Nasjonens hele navn: ";
  cin >> navn;  // Henter inn navn

  cout << "Navnet til kontaktperson: ";
  cin >> kontaktPerson;   // Henter inn navn på kontaktperson

  cout << "Tlf nr. til kontaktperson: ";
  cin >> kontaktTlf;    // Henter kontaktpersons tlf nr

  display(); //DEBUG

}
void Nasjon::skrivTilFil(ofstream &utfil) {  // Skriver nasjondata til fil
  utfil << forkortelse << ','
        << navn << ','
        << antDeltagere << ','
        << kontaktPerson << ','
        << kontaktTlf << '\n';
}

void Nasjon::display() {  // Printer ut data om nasjon
  cout << endl << "Nasjonens navn: " << navn
       << "(" << forkortelse << ")" << endl
       << "Antall deltagere fra nasjonen: " << antDeltagere << endl
       << "Kontaktperson til landet: " << kontaktPerson << endl
       << "Telefon nummer: " << kontaktTlf << endl;
}

Nasjon::~Nasjon() {  // frigjører plass i minnet
  delete[] forkortelse;
}
