#include "Global.h"
#include <string>
#include <iostream>
#include <stdio.h>

using namespace std;

Nasjoner nasjoner;
Deltagere deltagere;
Grener grener;
Medaljer medaljer;
Poeng poeng;

char les() {    //  Leser og returnerer ETT upcaset tegn.
	char ch;
	cout << "\n\nKommando:  ";
	cin >> ch;  cin.ignore();
	return (toupper(ch));
}

//  Skriver ledetekst (t), leser og
//    returnerer et tall mellom min og max:
int les(char* t, int min, int max) {
	int tall;
	do {
		cout << '\t' << t << " (" << min << '-' << max << "):  ";
		cin >> tall;  cin.ignore();
	} while (tall < min || tall > max);
	return tall;
}

string les(string beskrivelse, int len) {
	string t;
	do {
		cout << '\t' << beskrivelse << "lengde: " << len << " karakterer";
		cin >> t;
	} while(t.length() != len);
	return t;
}
